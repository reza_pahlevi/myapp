Ext.define('MyApp.store.Personnel', {
    extend: 'Ext.data.Store',
    xtype: 'personnel',
    alias: 'store.personnel',
    storeId: 'personnel',
    autoLoad: true,
   // autoSync: true,

    fields: [
        'user_id','name','email','phone','jurusan','gambar', 
    ],
    
    proxy: {
        type: 'jsonp',
        api: {
            read    : "http://localhost/MyApp_php/personnel.php",
            create  : "http://localhost/MyApp_php/addPersonnel.php",
            update  : "http://localhost/MyApp_php/updatePersonnel.php",
            destroy  : "http://localhost/MyApp_php/removePersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty: 'error',
            url: 'simpsons'
        }        
    }
});
