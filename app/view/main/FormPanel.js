Ext.define('MyApp.view.main.FormPanel', {
    extend: 'Ext.form.Panel',
    xtype: 'Form-Rigistasi',
    //id: 'Form-Rigistasi',
    controller: 'Form-Register',
    //title: 'Registrasi',
    requires: [
        'MyApp.view.main.RegisterFormController',
        'Ext.form.FieldSet',
        'Ext.field.Email'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    id: 'basicform',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: 'Input Dataan Siswa',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'user_id',
                    label: 'NPM',
                    placeHolder: '153510357',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true,
                    id:'user_id'
                },
                {
                    xtype: 'textfield',
                    revealable: true,
                    name : 'name',
                    label: 'Nama',
                    placeHolder: 'Reza',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true,
                    id:'name'
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true,
                    id:'email'
                },
                {
                    xtype: 'textfield',
                    revealable: true,
                    name : 'phone',
                    label : 'No HP',
                    placeHolder: '08126862xxxx',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true,
                    id:'phone'
                },
                {
                    xtype: 'selectfield',
                    name: 'jurusan',
                    label: 'Jurusan',
                    required: true,
                    clearIcon: true,
                    id:'jurusan',
                    options: [
                        {
                            text: 'Teknik Sipil',
                            value: 'Teknik Sipil'
                        },
                        {
                            text: 'Teknik Perminyakan',
                            value: 'Teknik Perminyakan'
                        },
                        {
                            text: 'Teknik Informatika',
                            value: 'Teknik Informatika'
                        }
                    ]
                },
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Reset',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('basicform').reset();
                    }
                },
                {
                    text: 'Simpan',
                    ui: 'action',
                    formBind: true,
                    handler:'onRegisterClick',
                },
            ]
        }
    ]
});