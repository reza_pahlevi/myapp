/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },
    onRowSelected: function ( record, element, rowIndex, e, eOpts ){
        alert(element.data.name);
        console.log(element.data.name);
    }, 
    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },
    onRemoveClick: function (view, recIndex, cellIndex, item, e, record) {
        var id = Ext.getCmp('user_id').getValue();
        var store = Ext.getStore('personnel');
            store.Delete(id);
        var record = store.getById(id);
        Ext.Msg.confirm("Delete Confirmation","Are you sure to delete this record?",function(btn){
            if (btn == 'yes')
               record.erase({
                    success: function() {
                        store.remove(record);
                        store.sync();
                    }
                });                                     
        })            
    },
    onAddClick: function () {        
        var view = this.getView();
        view.getStore().insert(0, {user_id: '',name: '', email: '', phone: ''});
        view.findPlugin('rowediting').startEdit(0, 0);          
    }
});
