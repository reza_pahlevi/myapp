Ext.define('MyApp.view.main.RegisterFormController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.Form-Register',
    onRegisterClick: function () {  
        /*store = Ext.getCmp('mainlist');
        Ext.Msg.alert('Success', 'The new user is successfully added', function(){
            store.load();
        });*/
        var  user_id, name, email, phone, jurusan, store;
        user_id = Ext.getCmp('user_id').getValue();
        name  = Ext.getCmp('name').getValue();
        email = Ext.getCmp('email').getValue();
        phone = Ext.getCmp('phone').getValue();
        jurusan = Ext.getCmp('jurusan').getValue();

        store = Ext.getStore('personnel');
        store.insert(0, [{ user_id: user_id, name: name, email: email, phone: phone, jurusan: jurusan}]);
        store.sync();

        Ext.getCmp('basicform').reset();

        Ext.Msg.alert('Success', 'New user has been added.', Ext.emptyFn);

        console.log('store: ', store);        
    }
});
