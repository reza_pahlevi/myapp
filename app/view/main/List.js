Ext.define('MyApp.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'MyApp.store.Personnel'
    ],

    title: 'Data Siswa',
    plugins: {
        type: 'grideditable',
        triggerEvent: 'doubletap',
        enableDeleteButton: true,
        formConfig: null, // See more below

        defaultFormConfig: {
            xtype: 'formpanel',
            scrollable: true,
            items: {
                xtype: 'fieldset'
            }
        },

        toolbarConfig: {
            xtype: 'titlebar',
            docked: 'top',
            items: [{
                xtype: 'button',
                ui: 'decline',
                text: 'Batal',
                align: 'left',
                action: 'cancel'
            }, {
                xtype: 'button',
                ui: 'confirm',
                text: 'Simpan',
                align: 'right',
                action: 'submit'
            }]
        }
    },
    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Name',  dataIndex: 'name', width: 170, editable: true},
        { text: 'Email', dataIndex: 'email', width: 170, editable: true },
        { text: 'Phone', dataIndex: 'phone', width: 170,editable: true },
        { text: 'Jurusan', dataIndex: 'jurusan', width: 170,editable: true }

    ]
    /*listeners: {
        select: 'onItemSelected'
    },*/
});
