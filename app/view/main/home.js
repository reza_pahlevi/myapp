Ext.define('MyApp.view.home', {
    extend: 'Ext.tab.Panel',
    xtype: 'home',
    config: {
        tabBarPosition: 'bottom',
        items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'S I P A S'
                },
 
                html:[
                    "<br /><br />",
                    "<p><br><center>SELAMAT DATANG ADMIN SIPAS</center>",
                    "<br /><br />",
                    '<center><img src="resources/icon/uir.png" width="60%" class="my_gambar" /><center>',
                    '<br> MADRASAH ALIYAH HIDAYATUL MUTA ALLIM',
                    '<br>MENGKIRAU</p>'
                ].join("")
    }
});