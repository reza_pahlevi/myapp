/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('MyApp.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'Pendataan Siswa',

        loremIpsum: '<br><br><center><font size=4 color=blue>About Me</center><br>'+ 
        '<center><img src="resources/icon/re.jpg" width="60%" class="img" /><center>'+
        '<br>Nama: Reza Pahlevi'+
        '<br>Kelas: VA'+
        '<br>NPM: 173510642'+
        '<br>Hobby: Main Game'
    }
    //TODO - add data, formulas and/or methods to support your view
});